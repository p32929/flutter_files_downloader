## 1.0.14

Fixed bug where the callback **onDownloadCompleted** might not be called sometimes
